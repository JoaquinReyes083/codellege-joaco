# Portafolio Codellege

Este es mi portafolio de ejercicios realizados durante el curso Codellege 2019

## que hago?
1. CREAR LA CARPETA "public"
2. AÑADIR UN INDEX.HTML EN LA CARPETA PUBLIC
PUBLIC
    - La idea es que sea la pagina principal y tenga una lista de links a todos los trabajos que hemos hecho
3. GUARDAR Y ENVIAR LOS CAMBIOS A GITLAB, abre una terminal en tu carpeta y sigue los siguientes comandos:
    - git add <nombre de tus archivos>
    - git add README.md public/index.html .gitlab-ci.yml
    - git commit -m "Añadir pagina inicial"
    - git push 
 4. Añadir una carpeta por cada proyecto o ejercicio que hagamos hecho

## COMO REVISO?
1. Copia la URL de tu proyecto https://gitlab.com/JoaquinReyes083/plain-html
2. Convierte la URL de tu proyecto https://JoaquinReyes083.gitlab.io/codellege/
